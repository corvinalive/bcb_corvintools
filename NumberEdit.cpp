//---------------------------------------------------------------------------

#include <vcl.h>

#pragma hdrstop

#include "NumberEdit.h"
#pragma package(smart_init)
//---------------------------------------------------------------------------
// ValidCtrCheck is used to assure that the components created do not have
// any pure virtual functions.
//

static inline void ValidCtrCheck(TNumberEdit *)
{
        new TNumberEdit(NULL);
}
//---------------------------------------------------------------------------
__fastcall TNumberEdit::TNumberEdit(TComponent* Owner)
        : TEdit(Owner)
{
   Font->Name="Courier New";
   Font->Size=10;
   Text=Value;
}
//---------------------------------------------------------------------------
namespace Numberedit
{
        void __fastcall PACKAGE Register()
        {
                 TComponentClass classes[1] = {__classid(TNumberEdit)};
                 RegisterComponents("Corvin", classes, 0);
        }
}
//---------------------------------------------------------------------------
 void __fastcall TNumberEdit::Change(void)
{
   TEdit::Change();
}
//---------------------------------------------------------------------------
void __fastcall TNumberEdit::KeyPress(char &Key)
{
   s=Text;
   if((isdigit(Key)!=0)||(Key=='-')||(Key=='+')||(Key==0x8))
      {
      TEdit::KeyPress(Key);
      return;
      }
   if(((Key=='e')||(Key=='E')||(Key=='�') || (Key=='�') )&&(NumberType==Double))
      {
      Key='E';
      TEdit::KeyPress(Key);
      return;
      }
   if(((Key=='.')||(Key==','))&&(NumberType==Double))
      {
      Key=DecimalSeparator;
      TEdit::KeyPress(Key);
      return;
      }
   Key=0;
}
//---------------------------------------------------------------------------
void __fastcall TNumberEdit::SetNumberType(TNumberType value)
{
   if(FNumberType != value)
      {
      FNumberType = value;
      Value=FValue;
      }
}
//---------------------------------------------------------------------------
void __fastcall TNumberEdit::SetValue(double value)
{
   if(NumberType==Integer) FValue=(int) value;
      else FValue=value;
   Text=FValue;
}
//---------------------------------------------------------------------------

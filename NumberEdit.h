//---------------------------------------------------------------------------

#ifndef NumberEditH
#define NumberEditH
//---------------------------------------------------------------------------
#include <SysUtils.hpp>
#include <Classes.hpp>
#include <Controls.hpp>
#include <StdCtrls.hpp>
//---------------------------------------------------------------------------
class PACKAGE TNumberEdit : public TEdit
{
public:
   enum TNumberType {Integer=0,Double=1};
private:
protected:
   TNumberType FNumberType;
   AnsiString s;
   double FValue;
   bool FIsValueValid;
   void __fastcall SetNumberType(TNumberType value);
   void __fastcall SetValue(double value);
   DYNAMIC void __fastcall Change(void);
   DYNAMIC void __fastcall KeyPress(char &Key);
public:
   __property bool IsValueValid  = { read=FIsValueValid };
   __fastcall TNumberEdit(TComponent* Owner);
__published:
   __property TNumberType NumberType  = { read=FNumberType, write=SetNumberType };
   __property double Value  = { read=FValue, write=SetValue };
};
//---------------------------------------------------------------------------
#endif
